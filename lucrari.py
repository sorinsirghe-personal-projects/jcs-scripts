#!/usr/bin/python3
import cx_Oracle
import pymysql
import telegram

contacts = {'Sorin': 'TELEGRAM_CHAT_ID',
            'Dan': 'TELEGRAM_CHAT_ID',
            'Proiectare': 'TELEGRAM_CHAT_ID',
            'Grup': 'TELEGRAM_CHAT_ID'
    }

# Oracle DB info
CONN_INFO = {
    'host': 'IP_ADDRESS',
    'port': 1521,
    'user': 'USERNAME',
    'psw': 'PASSWORD',
    'service': 'SERVICE'
    }

# String to connect to the Oracle DB
CONN_STR = '{user}/{psw}@{host}:{port}/{service}'.format(**CONN_INFO)

# First query to run to access the database
QUERY1 = "ALTER SESSION SET CURRENT_SCHEMA=jcs"

# Query to access the data in the Lucrari tab
QUERY2 = '''
    SELECT
      T .*
    FROM
      (
        SELECT
          A .id_lucrare,
          A .datai,
          A .status,
          b.numar_intern,
          b.clocation AS locatie,
          status_osuri,
          wo_closedon
        FROM
          nom_lucrari A
        LEFT JOIN vcontracte_all b ON A .id_ctr = b.id_ctr
        LEFT JOIN (
          SELECT
            stringagg (TO_CHAR(x.status_os)) AS status_osuri,
            stringagg (
              TO_CHAR (
                x.dataora_exec,
                'dd/mm/yy hh24:mi'
              )
            ) AS wo_closedon,
            id_lucrare
          FROM
            vos x
          WHERE
            NVL (id_lucrare, 0) <> 0
          GROUP BY
            id_lucrare
        ) D ON A .id_lucrare = D .id_lucrare
        WHERE
          A .sters = 0
      ) T
    where status=0
'''

#Query to access the last ID stored in the local database
QUERY3 = """
    SELECT
        id
    FROM
        lucrari
"""

def sendMessage(person, message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    for x in person:
        bot.send_message(chat_id=contacts[x], text=message)

def getID():
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    c.execute(QUERY3)
    return c.fetchone()['id']

def changeID(id):
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    update = "UPDATE `lucrari` SET `id`={};".format(id)
    c.execute(update)
    conn.commit()

try:
    # Initialize the db connection and get all the results
    conn = cx_Oracle.connect(CONN_STR)
    cursor = conn.cursor()
    cursor.execute(QUERY1)
    result = cursor.execute(QUERY2).fetchall()

    lastID = getID()
    newID = 0
    for x in result:
        if (x[0] > newID):
            newID = x[0]
    if newID > lastID:
        if (newID - lastID == 1):
            message = "Exista o lucrare noua cu ID-ul {} care trebuie procesata".format(newID)
        else:
            message = "Exista {} lucrari noi, ultima cu ID-ul {} care trebuie procesate.".format(
                newID - lastID, newID)
        sendMessage(['Grup', 'Dan'], message)
        changeID(newID)
except Exception as e:
    sendMessage(['Sorin'], 'Este o problema la lucrare.py.\n{}'.format(str(e)))
