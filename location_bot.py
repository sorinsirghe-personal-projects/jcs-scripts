#!/usr/bin/python3
import pymysql
import re
from telegram.ext import Updater
from telegram.ext import CommandHandler

def translate(x):
    # Transforms location code input into 6 digits building code
    re.sub("\D", "", x)
    x = x [-6:]
    x = x.rjust(6, '0')
    return x

def database(message):
    # Searches in the database for the input codes
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    final = []
    for x in message:
        c.execute("SELECT Code, Type, Coordinates \
                   FROM location \
                   WHERE Code LIKE '%{}';".format(translate(x)))
        valid = c.fetchall()
        if len(valid):
            for row in valid:
                final.append("{} {}\nhttp://google.com/maps/place/{}".format(row['Code'], row['Type'], row['Coordinates']))
        elif x != '':
            final.append("{} was not found in the database".format(x))
    conn.close()
    return final

def start(bot, update):
    # /start command for the bot
    bot.send_message(chat_id=update.message.chat_id,
                    text="""Use /location code to get the location for any code.
If you use /location 123 for example, you will get the link for building 000123.
You can also use /location 62 745 1000 to get the links for all 3 codes.""")

def location(bot, update, args):
    # /location command for the bot
    final = database(args)
    for x in final:
        update.message.reply_text(x)

# Initialize the bot
updater = Updater(token='TELEGRAM_BOT_TOKEN')
dispatcher = updater.dispatcher

# Add the commands to the bot
dispatcher.add_handler(CommandHandler('start', start))
dispatcher.add_handler(CommandHandler('location', location, pass_args=True))

updater.start_polling()
