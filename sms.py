#!/usr/bin/python3
import datetime
import re
import telegram
import pymysql
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

def sendMessage(message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    bot.send_message(chat_id='TELEGRAM_CHAT_ID', text=message)

class person:
    def __init__(self, phone, message, translated=''):
        self.phone = phone
        self.message = message
        self.translated = translated

class sms:
    def __init__(self):
        self.options = Options()
        self.options.add_argument("--headless")
        self.driver = webdriver.Chrome(options=self.options)
        self.lastDate = self.getLastDate()
        self.newestDate = self.lastDate
        self.validConversations = []
        self.complaints = []

    def getLastDate(self):
        try:
            conn = pymysql.connect(
                db='DATABASE',
                user='USERNAME',
                passwd='PASSWORD',
                host='localhost',
                cursorclass=pymysql.cursors.DictCursor)

            query = "SELECT datetime FROM sms"

            c = conn.cursor()
            c.execute(query)
            return c.fetchone()['datetime']
        except Exception as e:
            sendMessage("Eroare SMS, getLastDate.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def updateLastDate(self):
        try:
            conn = pymysql.connect(
                db='DATABASE',
                user='USERNAME',
                passwd='PASSWORD',
                host='localhost',
                cursorclass=pymysql.cursors.DictCursor)

            query = "UPDATE sms \
            SET datetime = '{}' \
            WHERE id = 1;".format(self.newestDate)

            c = conn.cursor()
            c.execute(query)
            conn.commit()
        except Exception as e:
            sendMessage("Eroare SMS, UpdateLastDate.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def getDateTime(self, string):
        try:
            months = {"Jan": 1,
                      "Feb": 2,
                      "Mar": 3,
                      "Apr": 4,
                      "May": 5,
                      "Jun": 6,
                      "Jul": 7,
                      "Aug": 8,
                      "Sep": 9,
                      "Oct": 10,
                      "Nov": 11,
                      "Dec": 12
                }

            expr = r"(\w{3}) (\d{1,2}), (\d{4}) (\d{1,2}:\d{1,2}:\d{1,2}) (AM|PM)"
            regex = re.search(expr, string)
            month = months[regex.group(1)]
            day = regex.group(2)
            year = regex.group(3)
            time = regex.group(4).split(":")
            if (regex.group(5) == "PM" and time[0] != "12"):
                hour = "{}".format(int(time[0]) + 12)
            else:
                hour = time[0]
            minutes = time[1]
            seconds = time[2]

            dt = "{}-{}-{} {}:{}:{}".format(year, month, day, hour, minutes, seconds)
            dt = datetime.datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
            return(dt)
        except Exception as e:
            sendMessage("Eroare SMS, getDateTime.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def mightyText(self):
        try:
            self.driver.get('https://mightytext.net/web8/')

            WebDriverWait(self.driver, 5).until(
             EC.presence_of_element_located((
              By.ID, 'Email')))
            self.driver.find_element_by_id('Email').send_keys(
                'EMAIL_ADDRESS')

            WebDriverWait(self.driver, 5).until(
             EC.presence_of_element_located((
              By.ID, 'next')))
            self.driver.find_element_by_id('next').click()

            WebDriverWait(self.driver, 5).until(
             EC.presence_of_element_located((
              By.ID, 'Passwd')))
            self.driver.find_element_by_id('Passwd').send_keys(
                'PASSWORD')

            WebDriverWait(self.driver, 5).until(
             EC.presence_of_element_located((
              By.ID, 'signIn')))
            self.driver.find_element_by_id('signIn').click()

            try:
                # Somtimes Google asks you to select which account to use after you
                # sign in. Attempts to select the sms account for 5 seconds.
                WebDriverWait(self.driver, 5).until(
                 EC.presence_of_element_located((
                  By.ID, 'approve_button')))
                self.driver.find_element_by_id('approve_button').click()
            except Exception as e:
                pass

            WebDriverWait(self.driver, 15).until(
             EC.presence_of_element_located((
              By.ID, 'navBarTabs')))
            allSMS = self.driver.find_element_by_id('navBarTabs')
            allSMS = allSMS.find_elements_by_tag_name('li')
            self.getNewSMS(allSMS)
            for i in range(len(self.validConversations)):
                # In order to load a conversation in Selenium, you need to click it
                mainScreen = self.validConversations[i]
                mainScreen.click()
                phone = self.driver.find_element_by_id(
                    'singleMessageContainer').find_element_by_class_name(
                    'messageThread').find_element_by_class_name(
                    'msg-thread-contact-info-container').find_element_by_class_name(
                    'contentPanelHeaderNum').text
                if (phone == "VOICEMAIL"):
                    # Ignores voice mail
                    continue
                conversation = self.driver.find_element_by_id(
                    'singleMessageContainer').find_element_by_class_name(
                    'messageThread').find_element_by_class_name(
                    'threadConversationHolder').find_elements_by_class_name(
                    'receivedText')
                conversation = conversation[::-1]
                message = self.readMessages(conversation)
                if (message == False):
                    continue
                elif (message != ''):
                    phone = re.sub(r"\+962", "0", phone)
                    self.complaints.append(person(phone=phone, message=message))
        except Exception as e:
            sendMessage("Eroare SMS, mightyText.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def getNewSMS(self, allConversations):
        # Reads all conversations and keeps those that are new
        try:
            for i in allConversations:
                convTime = self.getDateTime(i.get_attribute('innerHTML'))
                if (convTime > self.lastDate):
                    self.validConversations.append(i)
                    # Saves the newest timestamp
                    if (convTime > self.newestDate):
                        self.newestDate = convTime
                else:
                    break
        except Exception as e:
            sendMessage("Eroare SMS, getNewSMS.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def readMessages(self, conversation):
        # Reads all messages from a single conversation and if they are valid,
        # appends them to a single string
        try:
            if len(conversation) == 0:
                return False
            message = ''
            expr = r"data-ts=\"(.+)\" data-utc"
            for i in range(len(conversation)):
                sms = conversation[i].text
                regex = re.search(expr, conversation[i].get_attribute('innerHTML'))
                smsTime = self.getDateTime(regex.group(1))
                if (smsTime > self.lastDate):
                    # Removes the text that mentions how long ago the message has been received
                    sms = re.sub(r"\n(\d{1,2}) (min|sec)", '', sms)
                    message += sms
                    message += '\n'
                else:
                    break
            return message.strip('\n')
        except Exception as e:
            sendMessage("Eroare SMS, readMessages.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def translate(self, text):
        # Connects to Google Translate and translates arabic text
        try:
            text = re.sub(r"\n", "%0D%0A", text)
            url = "https://translate.google.com/#view=home&op=translate&sl=ar&tl=en&text={}".format(text)
            self.driver.get(url)
            WebDriverWait(self.driver, 5).until(
             EC.presence_of_element_located((
              By.CLASS_NAME, 'result-shield-container')))
            translatedText = self.driver.find_element_by_class_name(
                'result-shield-container').text
            return translatedText
        except Exception as e:
            sendMessage("Eroare SMS, translate.\n{}".format(str(e)))
            return "ERROR! Could not translate the text."

    def fillForm(self, person):
        # Connects to the JCS site and fills the support form
        try:
            self.driver.get("https://jcs.jo/en/contact-us/")
            WebDriverWait(self.driver, 10).until(
             EC.presence_of_element_located((
              By.NAME, 'your-name')))
            self.driver.find_element_by_name(
                'your-name').send_keys("SMS")

            self.driver.find_element_by_name(
                'your-email').send_keys("sms@jcs.jo")

            self.driver.find_element_by_name(
                'tel-172').send_keys(person.phone)

            checkboxes = self.driver.find_elements_by_name(
                'file-335[]')
            for cb in checkboxes:
                cb.click()

            text = "Original message:\n"
            text += person.message
            text += "\n\nTranslated message:\n"
            text += person.translated

            self.driver.find_element_by_name('your-message').send_keys(text)

            self.driver.find_element_by_class_name('wpcf7-submit').click()
        except Exception as e:
            sendMessage("Eroare SMS, fillForm.\n{}".format(str(e)))
            self.driver.quit()
            exit()

    def start(self):
        self.mightyText()
        if (len(self.complaints) != 0):
            for x in self.complaints:
                x.translated = self.translate(x.message)
                self.fillForm(x)
            self.updateLastDate()
        self.driver.quit()

def main():
    test = sms()
    test.start()

if __name__ == "__main__":
    main()
