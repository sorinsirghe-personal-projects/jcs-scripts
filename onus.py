#!/usr/bin/python3
import pymysql
import re
import requests
import datetime

conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

c = conn.cursor()

def read():
    date = datetime.datetime.now()

    source = requests.get("SERVER_IP/onu/activated_onu.php")

    total = re.findall(r"<tr>([\d\D]+?)<\/tr>", source.text)
    dict = {}
    for x in range(1,len(total)):
        values = re.findall(r">{1,2}([\d\D]*?)<\/td>", total[x])
        dict['olt'] = values[1].upper()
        dict['gcob'] = int(values[3])
        dict['pon'] = int(values[4])
        dict['pos'] = int(values[5])
        dict['status'] = int(values[6])
        dict['serial'] = values[7].upper()
        dict['tx'] = float(re.sub(r".*'error'>", "", values[8]))
        dict['rx'] = float(re.sub(r".*'error'>", "", values[9]))
        dict['down'] = int(values[10])
        dict['up'] = int(values[11])
        dict['temp'] = float(re.sub(r"&deg;C", "", values[13]))

        insert(dict, date)

def insert(dict, date):
    query = "INSERT INTO `onus` \
        (datetime, serial, olt, gcob, pon, position, status, transmit, receive, down, up, temp) \
        VALUES('{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, {}, {});".format(
        date, dict['serial'], dict['olt'], dict['gcob'], dict['pon'], dict['pos'], 
            dict['status'], dict['tx'], dict['rx'], dict['down'], dict['up'], dict['temp'])
    c.execute(query)

read()
conn.commit()
