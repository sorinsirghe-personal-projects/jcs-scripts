#!/usr/bin/python3
import cgi
import cgitb
import pymysql
import re

cgitb.enable()

def translate(x):
    re.sub("\D", "", x)
    x = x [-6:]
    x = x.rjust(6, '0')

    return x

conn = pymysql.connect(
    db='DATABASE',
    user='USERNAME',
    passwd='PASSWORD',
    host='localhost',
    cursorclass=pymysql.cursors.DictCursor)

c = conn.cursor()

form = cgi.FieldStorage()
text = form.getvalue("codeinput").split(' ')

print("Content-type:text/html\r\n")
print("<html>")
print("<body>")
for x in text:
    c.execute("SELECT Code, Type, Coordinates \
               FROM location \
               WHERE Code LIKE '%{}';".format(translate(x)))
    valid = c.fetchall()
    conn.close()
    if len(valid):
        for row in valid:
            print("<p>{} {}<br>http://google.com/maps/place/{}</p<br>".format(row['Code'], row['Type'], row['Coordinates']))
    elif x != '':
        print("<p>{} nu a fost gasit in baza de date</p><br>".format(x))

print("</body>")
print("</html>")
