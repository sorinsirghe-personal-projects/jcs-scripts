<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'Exception.php';
require 'PHPMailer.php';
require 'SMTP.php';

function confirm($mesaj){
  require_once('./httpful.phar');

  $url = 'https://api.telegram.org/botTELEGRAM_BOT_TOKEN/sendMessage';

  $response = \Httpful\Request::post($url)
    ->sendsJson()
    ->expectsJson()
    ->body(json_encode(array(
      "chat_id" => "TELEGRAM_CHAT_ID",
      "text" => "$mesaj",
    )))
    ->sendIt();
}

$mail = new PHPMailer;
$mail->isSMTP();

$subject = $_GET['subject'];

$mail->SMTPDebug = 2;
$mail->Host = 'mail.jcs.jo';
$mail->Port = 587;
$mail->SMTPAuth = true;
$mail->Username = 'USERNAME';
$mail->Password = 'PASSWORD';
$mail->setFrom('EMAIL_ADDRESS', 'Mail Bot');
$mail->addAddress('RECEIVED_EMAIL_ADDRESS', 'Dan Craciun');
$mail->Subject = $subject;
$mail->Body = "Mesaj trimis de bot";
if (!$mail->send()) {
    $mesaj = $subject . " nu a putut fi trimis\n" . $mail->ErrorInfo;
    confirm($mesaj);
} else {
    $mesaj = $subject . " trimis cu success!";
    confirm($mesaj);
}
