#!/usr/bin/python3
import re
import time
import telegram
from requests import session
from requests.auth import HTTPBasicAuth
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

payload = {'ipaddr': 'APP_URL',
           'user': 'USERNAME',
           'password': 'PASSWORD',
           'loginId': 'ContentPlaceHolder1_tn',
           'pswdId': 'ContentPlaceHolder1_tp',
           'frameId': 'i1'}

def sendMessage(person, message): # person must be a list
    contacts = {'Sorin': 'TELEGRAM_USER_ID',
                'Dan': 'TELEGRAM_USER_ID',
                'Monitorizare': 'TELEGRAM_USER_ID'
                }
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    for x in person:
        bot.send_message(chat_id=contacts[x], text=message)

def logIn(driver, ip, user, pswd, logId, pswdId, frame):
    # Will access the given ip, switch to the frame containing the login
    # inputs and log in using the given user name and password
    try:
        driver.get(ip)
        driver.switch_to.frame(driver.find_element_by_id(frame))
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, logId)))
        driver.find_element_by_id(logId).send_keys(user)
        driver.find_element_by_id(pswdId).send_keys(pswd, Keys.RETURN)
        return True
    except Exception as e:
        sendMessage(['Sorin'], 'Bot-ul nu se poate conecta la Fiber pentru verificarea de clienti business.\n' + str(e))
        return False

def clientsInNagios():
    # Get a list with the business clients monitored in Nagios
    try:
        with session() as a:
            url = 'NAGIOS_URL'
            response = a.get('NAGIOS_URL', auth=HTTPBasicAuth('NAGIOS_USERNAME', 'NAGIOS_PASSWORD'))
            regex = re.findall("[0-9]{14}", response.text)
            return (list(set(regex)))
    except Exception as e:
        sendMessage(['Sorin'], 'Nu se poate accesa Nagios pentru verificarea de clienti business.\n' + str(e))
        return False

def clientsInWiki():
    # Get a list with the business clients in the MediaWiki page
    try:
        with session() as a:
            url = 'MEDIAWIKI_URL'
            response = a.get(url)

        # Client codes are represented as a 14 digit number
        return(re.findall('([0-9]{14})<', response.text))
    except Exception as e:
        sendMessage(['Sorin'], 'Nu se poate accesa Wiki pentru verificarea de clienti business.\n' + str(e))
        return False

def clientsInFiber(driver):
    try:
        # Waits until the navigation bar is visible and clicks on 
        # Network, then on Apartments
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'linetwork')))
        driver.find_element_by_id('linetwork').click()
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'aapartments')))
        driver.find_element_by_id('aapartments').click()

        # Clicks on the search button
        driver.find_element_by_id('ContentPlaceHolder1_A2').click()

        # Waits until the page has loaded, inputs the details to find business clients
        # and clicks on the search button
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'ContentPlaceHolder1_lerr')))
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'ContentPlaceHolder1_tb_abon')))
        driver.find_element_by_id('ContentPlaceHolder1_tb_abon').send_keys('bus')
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'ContentPlaceHolder1_tb_nrinreg')))
        driver.find_element_by_id('ContentPlaceHolder1_tb_nrinreg').send_keys(500)
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'ContentPlaceHolder1_bcaut')))
        driver.find_element_by_id('ContentPlaceHolder1_bcaut').click()

        # Reads the data from the table
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
             By.ID, 'ContentPlaceHolder1_grid1_ob_grid1BodyContainer')))
        table = driver.find_element_by_id(
            "ContentPlaceHolder1_grid1_ob_grid1BodyContainer").find_element_by_class_name(
            'ob_gBICont').find_element_by_class_name('ob_gBody')
        rows = []
        rows += table.find_elements_by_class_name('ob_gR')
        rows += table.find_elements_by_class_name('ob_gRA')

        clients = []
        for x in rows:
            building = x.find_elements_by_class_name(
                'ob_gC')[2].find_element_by_class_name(
                'ob_gCc1').find_element_by_class_name('ob_gCc2').text
            apartment = x.find_elements_by_class_name(
                'ob_gCW')[2].find_element_by_class_name(
                'ob_gCc1').find_element_by_class_name('ob_gCc2').text
            clients.append(building+apartment)
        return(clients)
    except Exception as e:
        sendMessage(['Sorin'], 'Bot-ul nu a putut procesa clinetii business din Fiber.\n' + str(e))
        return False

def fiberNagDifference(clFib, clNag, message):
    # Compares the clients in Fiber (ERP system) and the clients in Nagios
    fiber = True
    nagios = True

    for x in clFib:
        if not (x in clNag):
            message += '{} este client business in Fiber si nu este monitorizat in Nagios.\n'.format(x)
            fiber = False
    if fiber:
        message += 'Nu exista clienti business in Fiber care sa nu fie monitorizati in Nagios.\n'

    message += '\n\n'

    for x in clNag:
        if not (x in clFib):
            message += '{} este monitorizat in Nagios, dar nu este client business in Fiber.\n'.format(x)
            nagios = False
    if nagios:
        message += 'Nu exista clienti monitorizati in Nagios care sa nu fie clienti business in Fiber.\n'

    message += '\n\n'

    return message

def nagWikiDifference(clNag, clWiki, message):
    # Compares the clients in Wiki and the clients in Nagios
    nagios = True
    wiki = True

    for x in clNag:
        if not (x in clWiki):
            message += '{} este monitorizat in Nagios si nu are pagina in Wiki.\n'.format(x)
            nagios = False
    if nagios:
        message += 'Nu exista clienti monitorizati in Nagios care sa nu aiba pagina in Wiki.'

    message += '\n\n'

    for x in clWiki:
        if not (x in clNag):
            message += '{} are pagina in Wiki, dar nu este monitorizat in Nagios.\n'.format(x)
            wiki = False
    if wiki:
        message += 'Nu exista clienti care sa aibe pagina in Wiki si care sa nu fie monitorizati in Nagios.'

    message += '\n\n'

    return message

def fibWikiDifference(clFib, clWiki, message):
    # Compares the clients in Fiber and Wiki
    fiber = True
    wiki = True

    for x in clFib:
        if not (x in clWiki):
            message += '{} este client business in Fiber si nu are pagina in Wiki.\n'.format(x)
            nagios = False
    if fiber:
        message += 'Nu exista clienti business in Fiber care sa nu aiba pagina in Wiki.'

    message += '\n\n'

    for x in clWiki:
        if not (x in clFib):
            message += '{} are pagina in Wiki, dar nu este client business in Fiber.\n'.format(x)
            wiki = False
    if wiki:
        message += 'Nu exista clienti care sa aibe pagina in Wiki si care sa nu fie clienti business in Fiber.'

    return message

nagiosClients = False
wikiClients = False
fiberClients = False

while(nagiosClients == False):
    try:
        nagiosClients = clientsInNagios()
    except Exception as e:
        time.sleep(5)

while(wikiClients == False):
    try:
        wikiClients = clientsInWiki()
    except Exception as e:
        time.sleep(5)

while (fiberClients == False):
    try:
        options = Options()
        options.add_argument("--headless")
        driver = webdriver.Chrome(options = options)
        if(logIn(driver, payload['ipaddr'], payload['user'], payload['password'],
                payload['loginId'], payload['pswdId'], payload['frameId'])):
            fiberClients = clientsInFiber(driver)
            driver.quit()
    except Exception as e:
        if driver:
            driver.quit()
        time.sleep(10)

message = 'Sunt {} clienti business in Fiber, {} clienti monitorizati in Nagios si {} pagini de clienti business in Wiki.\n\n\n'.format(
    len(fiberClients), len(nagiosClients), len(wikiClients))
sendMessage(['Monitorizare', 'Dan'], message)
message = fiberNagDifference(fiberClients, nagiosClients, "")
sendMessage(['Monitorizare', 'Dan'], message)
message = nagWikiDifference(nagiosClients, wikiClients, "")
sendMessage(['Monitorizare', 'Dan'], message)
message = fibWikiDifference(fiberClients, wikiClients, "")
sendMessage(['Monitorizare', 'Dan'], message)
