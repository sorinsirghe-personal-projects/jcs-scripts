#!/usr/bin/python3
import cgi
import cgitb
import re

cgitb.enable()

form = cgi.FieldStorage()
text = form.getvalue("acl")

if (len(text) != 0):
    pattern = "(\d)-(\d{1,2})-(\d{1,2})"
    acl = re.search(pattern, text)

    gcob = int(acl.group(2))
    pon = int(acl.group(3))
    if (gcob > 10):
        gcob -= 10

    if (pon < 16):
        gcob -= 1
    elif (pon == 16):
        pon = 0

    port = (gcob * 16) + pon
    mesaj = "OLT-FH{}-Port{}".format(acl.group(1), port)
else:
    mesaj = "Trebuie sa introduci ceva."

print("Content-type:text/html\r\n")
print("<html>")
print("<body>")
print("<h1>{}</h1>".format(mesaj))
print("</body>")
print("</html>")
