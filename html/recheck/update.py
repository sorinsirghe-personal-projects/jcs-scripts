#!/usr/bin/python3
import telegram
import pymysql
import cgi

def changeURL(url):
    # Update the url in the local database to be used later
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    query = "UPDATE `recheck` SET `url`='{}';".format(url)
    c.execute(query)
    conn.commit()
    c.close()
    conn.close()

def sendMessage( message):
    # Sends a message via the Telegram bot "Reminder Bot" to "Agent Monitorizare"
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    bot.send_message(chat_id='TELEGRAM_CHAT_ID', text=message)

# Gets the value from the input field "recheck" from the HTML page
form = cgi.FieldStorage()
text = form.getvalue("recheck")

# A valid URL has to be from MEDIAWIKI_IP
if (text.startswith("MEDIAWIKI_IP/mediawiki")):
    changeURL(text)
    sendMessage("URL-ul introdus {} este valid si acceptat!\n\nBot-ul a fost activat!".format(text))
else:
    changeURL("invalid")
    sendMessage("URL-ul introdus {} NU este valid!\n\nBot-ul a fost dezactivat!".format(text))

# Prints something on the page so it won't show an error
print("Content-type:text/html\r\n")
print("<html>")
print("<head>")
print("<title>Recheck</title>")
print("</head>")
print("<body>")
print("<p>Verifica in telegram</p>")
print("</body>")
print("</html>")
