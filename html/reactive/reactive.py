#!/usr/bin/python3
import subprocess
import re
import cgi
import cgitb

cgitb.enable()

def filter(input):
    regex = '(\d{9})'
    result = re.findall(regex, input)
    return result

def ping(contract):
    p = subprocess.Popen('ping -c 1 -W 1.5 {}.jcsfiberlink.net'.format(contract), stdout=subprocess.PIPE, shell=True)
    p.wait()
    if p.poll():
        return True
    else:
        return False

form = cgi.FieldStorage()
text = form.getvalue("reactive")

print("Content-type:text/html\r\n")
print("<html>")
print("<head>")
print("<title>Ping</title>")
print("<script>")
print("function reload() {")
print("  location.reload();")
print("}")
print("setInterval(reload, 5000);")
print("</script>")
print("</head>")
print("<body>")
for contract in filter(text):
    if not ping(contract):
        print("<p>{} <font color='green'>raspunde la ping!</font></p>".format(contract))
    else:
        print("<p>{} <font color='red'>nu raspunde la ping!</font></p>".format(contract))
print("</body>")
print("</html>")
