#!/usr/bin/python3
import cgi
import cgitb
import pymysql

def next():
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()

    query = "SELECT `contact`, `data`, `mesaj`\
          FROM `alert`\
          WHERE facut=0\
          ORDER BY data\
          LIMIT 5;"

    c.execute(query)
    print("    Urmatoarele alerte:<br><br>")
    for x in c.fetchall():
        print("    {}   {}   {}<br>".format(
        x['data'], x['contact'],  x['mesaj']))

print("Content-type:text/html\r\n")
print("<html>")
print("<head>")
print("<title>Telegram Alarm</title>")
print("</head>")
print("<body>")
print("<!DOCTYPE html>")
print("<html>")
print("  <head>")
print("    <title>Telegram Alarm</title>")
print("  </head>")
print("  <body>")
print("    Data trebuie sa fie de forma an luna zi si sa foloseasca \"-\" ca separator. (exemple: 2020-02-20 pentru 20 februarie 2020, 2019-7-3 pentru 3 iulie 2019)<br>")
print("    Ora trebuie sa fie de forma ora minute si sa foloseasca \":\" ca separator (exemple: 20:50 pentru 8 seara si 50 minute, 5:1 pentru 5 dimineata si un minut)<br><br><br>")
print("    <form action=\"validate.py\" method=\"post\" enctype=\"multipart/form-data\" target=\"_blank\">")
print("    Data:<br><br><input type=\"text\" maxlength=\"10\" size=\"11\" name=\"data\" required><br><br>")
print("    Ora:<br><br><input type=\"text\" maxlength=\"5\" size=\"6\" name=\"ora\" required><br><br>")
print("    Mesaj:<br><br><input type=\"textarea\" maxlength=\"4096\" size=\"120\" name=\"mesaj\" required><br><br>")
print("    Cine: <br><br><select name=\"cine\" size = \"5\" required>")
print("            <option value=\"Monitorizare\">Monitorizare</option>")
print("            <option value=\"Dispecerat\">Dispecerat</option>")
print("            <option value=\"Activari\">Activari</option>")
print("            <option value=\"Proiectare\">Proiectare</option>")
print("            <option value=\"Dan\">Dan</option>")
print("          </select><br><br>")
print("    <input value=\"Adauga alarma\" type=\"submit\">")
print("    </form>")

next()

print("  </body>")
print("</html>")
