#!/usr/bin/python3
import cgi
import cgitb
import datetime
import pymysql
import re

conn = pymysql.connect(
    db='DATABASE',
    user='USERNAME',
    passwd='PASSWORD',
    host='localhost',
    cursorclass=pymysql.cursors.DictCursor)

cgitb.enable()

def validate():
    # Loads the values from the form input fields
    form = cgi.FieldStorage()

    data = form.getvalue("data")
    ora = form.getvalue("ora")
    mesaj = form.getvalue("mesaj")
    contact = form.getvalue("cine")

    # Validates the datetime by creating a datetime object
    try:
        time = data + ' ' + ora
        timestamp = datetime.datetime.strptime(time, '%Y-%m-%d %H:%M')
        if (timestamp < datetime.datetime.now()):
            print("Data si ora nu pot fi in trecut!")
            return False
    except:
        print("Formatul de data si/sau ora nu este valid!")
        return False

    # Ensures there is "\" in the message string
    if ("\\" in mesaj):
        print("Mesajul contine caracterul invalid \"\\\"!")
        return False

    # Inserts the validated data from the form into the database and informs the user
    c = conn.cursor()
    mesaj2 = mesaj.replace("'", "\\'")
    c.execute ("INSERT into alert \
        (data, mesaj, contact, facut) \
        VALUES ('{}', '{}', '{}', 0);".format(
            timestamp, mesaj2, contact))
    conn.commit()
    print("Mesajul \"{}\" pentru {} din data de {} a fost procesat cu succes!".format(
        mesaj, contact, timestamp))

print("Content-type:text/html\r\n")
print("<html>")
print("<head>")
print("<title>Telegram Alarm</title>")
print("</head>")
print("<body>")

validate()

print("</body>")
print("</html>")
