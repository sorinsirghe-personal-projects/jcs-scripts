#!/usr/bin/python3
import cgi
import telegram
import re
from requests import session

def sendMessage(person, message): # person trebuie sa fie o lista
    contacts = {'Dan': 'TELEGRAM_USER_ID',
                'Monitorizare': 'TELEGRAM_USER_ID'
                }
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    for x in person:
        bot.send_message(chat_id=contacts[x], text=message)

def sesiune(url):
    with session() as a:
        response = a.get(url)
        return response.text

def tura_zi():
    # check if URL is valid and if so, return HTML content of page
    form = cgi.FieldStorage()
    url = form.getvalue("istoric")

    if (url.startswith("MEDIAWIKI_IP/mediawiki/index.php/Tura_")):
        html_tura = sesiune(url)
        return html_tura
    else:
        return False

def evenimente(html):
    # Return a list with all the events in a given day
    expression = r'title="([\w\-\ ]+\d{4}-\d{2}-\d{2}\ \d{2}:\d{2})">'
    r = re.findall(expression, html)

    return r

def verificare(fisa):
    # check if the event is added to the history page
    url_fisa = "MEDIAWIKI_IP/mediawiki/index.php/" + fisa.replace(" ", "_")

    html_fisa = sesiune(url_fisa)

    expression = r'(\/mediawiki\/index\.php\/[iI]storic[\w+\-]+)'
    r = re.findall(expression, html_fisa)
    if len(r) == 0:
        return False
    url_istoric = "MEDIAWIKI_IP" + r[0]
    html_istoric = sesiune(url_istoric)

    r = re.findall(fisa, html_istoric, re.I)

    # return True if the event is added to the history page
    if len(r) != 0:
        return True
    else:
        return False

print("Content-type:text/html\r\n")
print("<html>")
print("<head>")
print("<title>Istoric Tura zi</title>")
print("</head>")
print("<body>")

html_tura = tura_zi()
if html_tura:
    fise = evenimente(html_tura)
    fise_uitate = []
    for fisa in fise:
        if (not verificare(fisa)):
            fise_uitate.append(fisa)

    if (len(fise_uitate) == 0):
        sendMessage(['Monitorizare', 'Dan'], "Toate cele {} fise sunt adaugate in istoric.".format(len(fise)))
    else:
        for fisa in fise_uitate:
            sendMessage(['Monitorizare', 'Dan'], "{} nu a fost adaugata in istoric!".format(fisa))
    print("Rezultatul interogarii va fi trimis pe Telegram la Monitorizare si Dan.")
else:
    print("URL-ul introdus nu este valid!")

print("</body>")
print("</html>")
