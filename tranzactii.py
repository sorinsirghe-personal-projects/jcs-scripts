#!/usr/bin/python3
import cx_Oracle
import telegram

contacts = {'Sorin': 'TELEGRAM_CHAT_ID',
            'Dan': 'TELEGRAM_CHAT_ID',
            'Monitorizare': 'TELEGRAM_CHAT_ID'
  }

# Oracle DB info
CONN_INFO = {
    'host': 'IP_ADDRESS',
    'port': 1521,
    'user': 'USERNAME',
    'psw': 'PASSWORD',
    'service': 'SERVICE'
    }

# String to connect to the Oracle DB
CONN_STR = '{user}/{psw}@{host}:{port}/{service}'.format(**CONN_INFO)

# First query to run to access the database
QUERY1 = "ALTER SESSION SET CURRENT_SCHEMA=jcsweb"

# Query to access the data in the Commercial - Tranzactii Tab
QUERY2 = '''
SELECT DATAORA, CONTRACT, IP, RASPUNS, INREGISTRAT FROM TRANZACTII WHERE RASPUNS=0 AND INREGISTRAT=0
'''

def sendMessage(person, message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    for x in person: # Solutie temporara pentru cat vrea Dan sa primeasca notificarile
        bot.send_message(chat_id=contacts[x], text=message)

try:
    # Initialize the db connection
    conn = cx_Oracle.connect(CONN_STR)
    cursor = conn.cursor()
    cursor.execute(QUERY1)
    result = cursor.execute(QUERY2).fetchall()
    if (len(result) != 0):
        sendMessage(['Monitorizare', 'Dan'], 'Exista intrari in Comercial -> Tranzactii. ' + str(result))
    conn.close()

except Exception as e:
    sendMessage(['Sorin', 'Dan'], 'Este o problema la tranzactii.py.\n{}'.format(str(e)))
