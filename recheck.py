#!/usr/bin/python3
import telegram
import pymysql
import re
import datetime
from requests import session

def getURL():
    # Returns the URL from the database
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    query = "SELECT url FROM recheck;"
    c.execute(query)
    result = c.fetchone()
    c.close()
    conn.close()
    return result

def sendMessage(message):
    # Sends a message via the Telegram bot "Reminder Bot" to "Agent Monitorizare"
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    bot.send_message(chat_id='TELEGRAM_CHAT_ID', text=message)

url = getURL()['url']
if (url != "invalid"):
    now = datetime.datetime.now()
    # Connects to the URL and saves the HTML of the page
    with session() as a:
        response = a.get(url)
    # Searches for a date in the format YYYY-MM-DD HH:MM and saves the results
    dates = re.findall(
        r"((?=RECHECK\s?)\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}|\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{1,2}(?=\s?RECHECK))", response.text)
    for date in dates:
        dateobj = datetime.datetime.strptime(date, "%Y-%m-%d %H:%M")
        # If a datetime result is from the past, it will send a message to Agent Monitorizare via Telegram
        if (dateobj <= now):
            sendMessage("Ai un recheck pentru data si ora {}".format(date))
            break
