#!/usr/bin/python3
import cx_Oracle
import telegram

contacts = {'Sorin': 'TELEGRAM_CHAT_ID',
            'Dan': 'TELEGRAM_CHAT_ID',
            'Activari': 'TELEGRAM_CHAT_ID',
            'Dispecer': 'TELEGRAM_CHAT_ID',
            'Grup': 'TELEGRAM_CHAT_ID',
  }

# Oracle DB info
CONN_INFO = {
    'host': 'IP_ADDRESS',
    'port': 1521,
    'user': 'USERNAME',
    'psw': 'PASSWORD',
    'service': 'SERVICE'
    }

# String to connect to the Oracle DB
CONN_STR = '{user}/{psw}@{host}:{port}/{service}'.format(**CONN_INFO)

# First query to run to access the database
QUERY1 = "ALTER SESSION SET CURRENT_SCHEMA=jcs"

# Query to access the data in the Abonati de reconectat tab
QUERY2 = '''
SELECT
    numar_intern
  FROM
    (
      SELECT
        T .ID_CTR,
        T .NUMAR_INTERN,
        T .activ,
        T .incetat,
        c.sold,
        c.avans,
        ap.cstatus_onu,
        (
          CASE
          WHEN statusonu = 4 THEN
            0
          ELSE
            1
          END
        ) AS deconectat,
        ap.id_onu,
        E .cod
      FROM
        vcontracte_all T
      LEFT JOIN vapartment ap ON T .id_ctr = ap.id_ctr
      LEFT JOIN (
        SELECT
          cod,
          (
            CASE
            WHEN totctva > totplata THEN
              totctva - totplata
            ELSE
              0
            END
          ) AS sold,
          (
            CASE
            WHEN totctva < totplata THEN
              - totctva + totplata
            ELSE
              0
            END
          ) AS avans
        FROM
          (
            SELECT
              cod,
              id_part,
              SUM (totctva) AS totctva,
              SUM (totplata) AS totplata
            FROM
              jv2007 T
            WHERE
              sters = 0
            AND id_fdoc NOT IN (42, 14)
            GROUP BY
              cod,
              id_part
          )
      ) c ON c.cod = T .numar_INTERN
      LEFT JOIN (
        SELECT DISTINCT
          cod
        FROM
          jv2007
        WHERE
          sters = 0
        AND id_fdoc = 25
        AND dataact >= SYSDATE - 5
      ) E ON E .cod = T .numar_INTERN
    )
  WHERE
    (
      (
        incetat = 0
        AND cstatus_onu IS NOT NULL
      )
      OR (
        incetat = 1
        AND activ = 1
        AND cod IS NOT NULL
      )
    )
  AND deconectat = 1
  AND sold <= 1
'''

def sendMessage(person, message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    for x in person:
        bot.send_message(chat_id=contacts[x], text=message)

try:
    # Initialize the db connection
    conn = cx_Oracle.connect(CONN_STR)
    cursor = conn.cursor()
    cursor.execute(QUERY1)
    result = cursor.execute(QUERY2).fetchall()
    if (len(result) > 30):
        sendMessage(['Grup', 'Dan'], 'Exista mai mult de 30 de intrari in abonati de reconectat. Probabil este o problema.')
    elif (len(result) == 1):
        sendMessage(['Grup', 'Dan'], 'Exista un singur contract in abonati de reconectat care trebuie procesat: {}.'.format(result[0][0]))
    elif (len(result) != 0):
        sendMessage(['Grup', 'Dan'], 'Exista {} contracte in abonati de reconectat care trebuie procesate.'.format(len(result)))
        for x in result:
            sendMessage(['Grup', 'Dan'], '{} trebuie reconectat.'.format(x[0]))
except Exception as e:
    sendMessage(['Sorin'], 'Este o problema la recon.py.\n{}'.format(str(e)))
