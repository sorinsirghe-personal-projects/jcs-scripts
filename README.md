# JCS Scripts

A collection of scripts I developed in Python while working for JCS (Jordanian Cable Services)


## Python Scripts that were hosted on a server and made with Python and CGI

### alarm
Store an alarm/reminder in a database and send it via Telegram bots

### business
Check if there are any differences between the clients with business subscriptions in the ERP system, the business clients monitored in the Nagios platform and the business clients with pages in Mediawiki

### calculator
Transform an ONU's access list from one format to a more readable format

### istoric
Check if the events in a day have been added to their respective history pages

### location
Receive a list of location codes and return Google Maps links with their coordinates

### mail
Web interface to quickly send start/end shift emails

### reactive
Parse a string for contract codes and ping them

### recheck
Take a MediaWiki URL check if there are any assignments that are due

## Python scripts, mostly made to use the Telegram API and requests

### addComplaint.py
Open a complaint in the ERP System using Selenium

### intervale.py
Parse the complaints list, check if any of them are due and send a list with all appointments

### location_bot.py
Telegram bot that receives a list of location codes and returns Google Maps link with their coordinates

### location_bot.sh
Shell script that checks if location_bot.py is running. If not, it will start it

### lucrari.py
Send a Telegram message when there are new work orders

### mutare.py
Senda Telegram message when there are new movement requests from clients

### onus.py
Update the databse with ONU data

### pamir.py
Use requests to check a service for DDOS attacks

### recheck.py
Send a Telegram message when there are assignments due

### recon.py
Send a Telegram message when there are new reconnection requests

### reminder_bot.py
Check if there are any reminders due and send them

### reminder_custom.py
Send a Telegram message with given message and chat
