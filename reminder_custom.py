#!/usr/bin/python3
import telegram
import sys

def sendMessage(person, message):
    contacts = {"grup": 'TELEGRAM_CHAT_ID',
                "dispecerat": 'TELEGRAM_CHAT_ID',
                "monitorizare": 'TELEGRAM_CHAT_ID'
                }

    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    bot.send_message(chat_id=contacts[person], text=message)

sendMessage(sys.argv[1], sys.argv[2])
