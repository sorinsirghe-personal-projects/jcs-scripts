#!/usr/bin/python3

import telegram
import re
import time
from requests import session
from datetime import date
from operator import itemgetter
from collections import OrderedDict

# Function to send a message via the telegram bot
def sendMessage(message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    bot.send_message(chat_id='TELEGRAM_CHAT_ID', text=message)

# returns the html source code of a webpage
def get_html(url):
    with session() as a:
        response = a.get(url)
        return response.text

# returns a list of all location codes and their comment section
def get_complaints():
    expr = r"<td>(\d{14}).*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*true\">(.*)"
    text = get_html('PAGE_IP/dispatch/complaints.php')
    complaints = re.findall(expr, text)
    return complaints

def main():
    # Dictionary with short and long month names
    months = {
        "jan": 1,
        "january": 1,
        "feb": 2,
        "february": 2,
        "mar": 3,
        "march": 3,
        "apr": 4,
        "april": 4,
        "may": 5, # long and short names are the same
        "june": 6, # long and short names are the same
        "july": 7, # long and short names are the same
        "aug": 8,
        "august": 8,
        "sept": 9,
        "september": 9,
        "oct": 10,
        "october": 10,
        "nov": 11,
        "november": 11,
        "dec": 12,
        "december": 12
    }

    # Dictionary with all available appointment intervals
    intervals = {
        '08:00-10:00': [],
        '09:00-11:00': [],
        '10:00-12:00': [],
        '12:00-14:00': [],
        '13:00-15:00': [],
        '15:30-17:30': [],
        '16:30-18:30': [],
        '17:30-19:30': [],
        '19:30-21:30': [],
        '20:30-22:30': [],
        '21:30-23:30': []
         }

    complaints = get_complaints()

    # Searches in the comment of a code for the day, month and interval when we can send a technician
    expr = r"Send technician on (\d{1,2}) ([a-zA-Z]{3,9}) between (\d{1,2}\:\d{2}[and\ \-]{1,5}\d{1,2}\:\d{2})"

    for code, comment in complaints:
        appointment = re.findall(expr, comment)
        # for an interval to be valid, it must be alone
        # there are cases when there are multiple intervals
        # in the comment section, sometimes from the previous today.
        # Those are considered invalid
        if (len(appointment) == 1):
            month = appointment[0][1]
            if (month.lower() in months.keys()):
                month = months[month.lower()]
                day = appointment[0][0]
                year = date.today().year
                # if the appointment is for today, it saves it
                if (date(year, month, int(day)) == date.today()):
                    interval = appointment[0][2].replace(" ", "")
                    interval = interval.replace("and", "-")
                    intervals[interval].append(code)
        elif (len(appointment) > 1):
            sendMessage("{} are mai multe intervale trecute!".format(code))

    message = ""
    sorted_intervals = OrderedDict(
        sorted(intervals.items(), key=itemgetter(0)))
    for key, value in sorted_intervals.items():
        if (len(value) != 0):
            text = "Pentru intervalul {} avem codurile:".format(key)
            for code in value:
                text = text + "\n" + code
        else:
            text = "Pentru intervalul {} nu avem coduri.".format(key)
        # telegram has a message limit of 4096 bytes
        if (len(message) + len(text) < 4096):
            message = message + "\n\n" + text
        else:
            sendMessage(message)
            message = text
    sendMessage(message)

if __name__=="__main__":
    main()

