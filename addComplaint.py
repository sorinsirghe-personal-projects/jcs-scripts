#!/usr/bin/python3
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

payload = {'ipaddr': 'ERP_IP',
           'user': 'USERNAME',
           'password': 'PASSWORD',
           'loginId': 'ContentPlaceHolder1_tn',
           'pswdId': 'ContentPlaceHolder1_tp',
           'frameId': 'i1',
           'navId': 'litechnical',
           'navComplainId': 'lideranjamente',
           'addComplainBtn': 'ContentPlaceHolder1_A4',
           'contractInput': 'ContentPlaceHolder1_itb_codclient',
           'submitBtn': 'ContentPlaceHolder1_badauga',
           'labelSubmit': 'ContentPlaceHolder1_lbmesaj',
           'submitBtn2': 'ContentPlaceHolder1_bmodifica',
           'reportedBy': 'ContentPlaceHolder1_itb_reported_by',
           'phone': 'ContentPlaceHolder1_itb_phone',
           'clientComplaint': 'ContentPlaceHolder1_itb_client_complaint'}


def addComplaint(complaint, reportedBy = 'NOC Romania',
    phone = '', clientComplaint = 'No net'):
    options = Options()
    options.add_argument("--headless")
    driver = webdriver.Chrome(options=options)
    #driver = webdriver.Chrome()
    driver.get(payload['ipaddr'])
    driver.switch_to.frame(driver.find_element_by_id(payload['frameId']))
    WebDriverWait(driver, 120).until(
     EC.presence_of_element_located((
      By.ID, payload['loginId'])))
    driver.find_element_by_id(payload['loginId']).send_keys(
        payload['user'])
    driver.find_element_by_id(payload['pswdId']).send_keys(
        payload['password'], Keys.RETURN)
    try:
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['navId'])))
        driver.find_element_by_id(payload['navId']).click()
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['navComplainId'])))
        driver.find_element_by_id(payload['navComplainId']).click()

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['addComplainBtn'])))
        driver.find_element_by_id(payload['addComplainBtn']).click()

        driver.switch_to.frame(driver.find_element_by_tag_name('iframe'))
        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['contractInput'])))
        driver.find_element_by_id(payload['contractInput']).send_keys(
            str(complaint))

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['submitBtn'])))
        driver.find_element_by_id(payload['submitBtn']).click()

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['submitBtn2'])))
        driver.find_element_by_id(payload['submitBtn2']).click()

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['reportedBy'])))
        driver.find_element_by_id(payload['reportedBy']).send_keys(
            str(reportedBy))

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['phone'])))
        driver.find_element_by_id(payload['phone']).send_keys(
            str(phone))

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['clientComplaint'])))
        driver.find_element_by_id(payload['clientComplaint']).send_keys(
            str(clientComplaint))

        WebDriverWait(driver, 120).until(
         EC.presence_of_element_located((
          By.ID, payload['submitBtn'])))
        driver.find_element_by_id(payload['submitBtn']).click()
    except Exception as e:
        pass
    driver.quit()

def main():
    addComplaint(486000948, "TEST", '', "TEST") #TEST ONU

if __name__ == "__main__":
    main()
