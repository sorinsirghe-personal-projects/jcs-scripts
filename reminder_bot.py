#!/usr/bin/python3
import pymysql
import datetime
import telegram

#Telegram contact ID's
contacts = {'Dan': 'TELEGRAM_CHAT_ID',
            'Proiectare': 'TELEGRAM_CHAT_ID',
            'Monitorizare': 'TELEGRAM_CHAT_ID',
            'Activari': 'TELEGRAM_CHAT_ID',
            'Dispecerat': 'TELEGRAM_CHAT_ID'
    }

def sendMessage(person, message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    bot.send_message(chat_id=contacts[person], text=message)

# Marks a reminder as done once it has been sent
def update(id):
    query = "UPDATE alert \
    SET facut = 1 \
    WHERE id = {}".format(id)
    c.execute(query)
    conn.commit()

conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

query1 = "SELECT * \
    FROM alert \
    WHERE facut = 0 \
    AND data <= '{}';".format(
        datetime.datetime.now())

c = conn.cursor()
c.execute (query1)
# Grabs all valid reminders, sends them and marks them as done 
alerts = c.fetchall()
for i in alerts:
    sendMessage(
        person=i['contact'], message=i['mesaj'])
    update(id=i['id'])
