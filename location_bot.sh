#!/usr/bin/zsh

result=`ps aux | grep -i "location_bot.py" | grep -v "grep" | wc -l`
if [ ! $result -ge 1 ]
   then
       . $HOME/.profile; /usr/bin/python <path_to_location_bot.py_script>
fi
