#!/usr/bin/python3

import re
import telegram
import pymysql
from requests import session
from requests.exceptions import ConnectionError

contacts = {'Sorin': 'TELEGRAM_CHAT_ID',
            'Dan': 'TELEGRAM_CHAT_ID',
            'Monitorizare': 'TELEGRAM_CHAT_ID'
    }

# Query to access the last date an attack occured from the local database
QUERY = """
    SELECT
        date
    FROM
        pamir
"""

def sendMessage(person, message):
    bot = telegram.Bot(token='TELEGRAM_BOT_TOKEN')
    for x in person:
        bot.send_message(chat_id=contacts[x], text=message)

def connect():
    # Connect to Pamir and get the html source of the history page
    urlAuth = 'https://pamir.retn.net/p3/auth/login'
    urlInfo = 'https://pamir.retn.net/p3/asn/UNIQUE+_ID/h'
    payload = {
        'username': 'USERNAME',
        'password': 'PASSWORD'
        }
    try:
        with session() as a:
            a.post(urlAuth, data=payload)
            response = a.get(urlInfo)
        return (response.text)
    except ConnectionError:
        pass
    except Exception as e:
        sendMessage(['Sorin'], 'Este o problema la pamir.py.\n' + str(e))
        return False

def getDate():
    # Get the last date an attack occured from the local database
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    c.execute(QUERY)
    return c.fetchone()['date']

def changeDate(date, ip, attack):
    # Update the last date an attack occured from the local database
    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost',
        cursorclass=pymysql.cursors.DictCursor)

    c = conn.cursor()
    update = "UPDATE `pamir` SET `date`={},`ip`='{}',`attack`='{}';".format(
        date, ip, attack)
    c.execute(update)
    conn.commit()

string = connect()
if (string):
    # Regex to extract the relevant data from the html source
    patternIP = r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{2})"
    patternAttackSize = r": (.*) at"
    patternDate = r"Date\((.*)\); "

    ip = re.search(patternIP, string).group(1)
    attack = re.search(patternAttackSize, string).group(1)
    date = float(re.search(patternDate, string).group(1))

    lastDate = getDate()
    if (date > lastDate):
        sendMessage(['Monitorizare', 'Dan'], 'Are loc un potential atac DDOS de {} pe {}. Verifica in Pamir.'.format(
            attack, ip))
        changeDate(date, ip, attack)
