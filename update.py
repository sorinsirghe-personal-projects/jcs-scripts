#!/usr/bin/python3
import pymysql
import cx_Oracle
import re

def update():
    # Oracle DB info
    CONN_INFO = {
        'host': 'IP_ADDRESS',
        'port': 1521,
        'user': 'USERNAME',
        'psw': 'PASSWORD',
        'service': 'SERVICE'
    }

    # String to connect to the Oracle DB
    CONN_STR = '{user}/{psw}@{host}:{port}/{service}'.format(**CONN_INFO)

    # First query to run to access the database
    query = "ALTER SESSION SET CURRENT_SCHEMA=jcs"

    conn = cx_Oracle.connect(CONN_STR)
    cursor = conn.cursor()
    cursor.execute(query)
    cursor.execute("SELECT \
        A.CODE, B.TYPE, A.COORDINATES \
    FROM \
        BUILDING A \
    INNER JOIN BUILDING_TYPES B \
        ON A.TYPE = B.ID")

    data = cursor.fetchall()
    conn.close()

    conn = pymysql.connect(
        db='DATABASE',
        user='USERNAME',
        passwd='PASSWORD',
        host='localhost')

    c = conn.cursor()

    c.execute('drop table if exists location')
    c.execute("CREATE TABLE location(Code VARCHAR(16), Type VARCHAR(32), Coordinates VARCHAR(128))")
    for row in data:
        try:
            if row[2] != None:
                c.execute("INSERT INTO location (`Code`, `Type`, `Coordinates`) VALUES ('{}', '{}', '{}')".format(
                    row[0], row[1], row[2].replace(" ", "")))
        except Exception as e:
            print(str(e))
            continue
    conn.commit()
    conn.close()

update()
