#!/usr/bin/python3
import cx_Oracle
import telegram

contacts = {'Sorin': 'TELEGRAM_CHAT_ID',
            'Dan': 'TELEGRAM_CHAT_ID',
            'Proiectare': 'TELEGRAM_CHAT_ID',
            'Grup': 'TELEGRAM_CHAT_ID'
    }

# Oracle DB info
CONN_INFO = {
    'host': 'IP_ADDRESS',
    'port': 1521,
    'user': 'USERNAME',
    'psw': 'PASSWORD',
    'service': 'SERVICE'
    }

# String to connect to the Oracle DB
CONN_STR = '{user}/{psw}@{host}:{port}/{service}'.format(**CONN_INFO)

# First query to run to access the database
QUERY1 = "ALTER SESSION SET CURRENT_SCHEMA=jcs"

# Query to access the data in the Packet change requests tab
QUERY2 = '''
    SELECT
      T . ID,
      T . DATA,
      T .verificat,
      C.numar_intern as contract
    FROM
      cereri_mutare T
    LEFT JOIN contracte c ON T .id_ctr = c.id_ctr
    WHERE
      NVL (T .sters, 0) = 0
    AND T .tip = 1
    AND verificat = 0
    AND validat = 1
    AND T . DATA <= SYSDATE
'''

def sendMessage(person, message):
    bot = telegram.Bot(token = 'TELEGRAM_BOT_TOKEN')
    for x in person:
        bot.send_message(chat_id = contacts[x], text = message)

try:
    # Initialize the db connection
    conn = cx_Oracle.connect(CONN_STR)
    cursor = conn.cursor()
    cursor.execute(QUERY1)
    result = cursor.execute(QUERY2).fetchall()

    if (len(result) == 1):
        sendMessage(['Grup', 'Dan'], 'Contractul {} are o cerere de schimbare de pachet care trebuie procesata sau verificata.'.format(result[0][3]))
    elif (len(result) != 0):
        sendMessage(['Grup', 'Dan'], 'Exista {} cereri de schimbare de pachet care trebuie procesate sau verificate'.format(len(result)))
except Exception as e:
    sendMessage(['Sorin'], 'Este o problema la speedreq.py.\n{}'.format(str(e)))
